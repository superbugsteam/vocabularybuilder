using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asp2017.Server.ConnectionString;
using Asp2017.Server.Data;
using LinqToDB.Data;
using Microsoft.AspNetCore.Mvc;

namespace Asp2017.Server.RestAPI
{

  [Route("api/[controller]")]
  public class WordsController : Controller
  {

    public string Database { get; set; } = "VocabularyBuilder";

    public WordsController()
    {
      DataConnection.DefaultSettings = new MySettings();
      LinqToDB.Common.Configuration.Linq.AllowMultipleQuery = true;
    }


    [HttpPost("")]
    public object All([FromBody] RequestModel request)
    {
      switch (request.Method)
      {
        case "get":
          return Get(request.Params);
      }
      return "";
    }


    public object Get(dynamic parameters)
    {
      var result = new List<Word>();
      var userID = (Guid)Guid.Parse(parameters.userID.ToString());
      var sortOrder = (string)parameters.sortOrder.ToString();

      using (var db = new DataConnection("SqlServer"))
      {

        var query = db.GetTable<Word>()
                         .AsQueryable();

        if (parameters.SelectionCriteria != null)
        {
          if (parameters.SelectionCriteria.filter != "")
          {
            var filter = (string)parameters.SelectionCriteria.filter.ToString();
            result = query.Where(c => c.En.IndexOf(filter) != -1 & c.UserID == userID).ToList();
          }
          else
          {
            if (sortOrder == "asc")
            {
              result = query.OrderBy(t => t.En).Where(h => h.UserID == userID).ToList();
            }
            else if (sortOrder == "desc")
            {
              result = query.OrderByDescending(w => w.En).Where(u => u.UserID == userID).ToList();
            }
          }
        }
      }

      return result;
    }
  }
}
