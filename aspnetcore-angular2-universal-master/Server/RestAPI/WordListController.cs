using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asp2017.Server.ConnectionString;
using Asp2017.Server.Data;
using LinqToDB;
using LinqToDB.Data;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Asp2017.Server.RestAPI
{

  [Route("api/[controller]")]
  public class WordListController : Controller
  {

    public WordListController()
    {
      DataConnection.DefaultSettings = new MySettings();
      LinqToDB.Common.Configuration.Linq.AllowMultipleQuery = true;
    }


    [HttpPost("")]
    public object All([FromBody] RequestModel request)
    {
      switch (request.Method)
      {
        case "get":
          return Get(request.Params);
        case "createWordList":
          return createWordList(request.Params);
        case "deleteWordList":
          return deleteWordList(request.Params);
        case "updateWordList":
          return updateWordList(request.Params);



      }
      return "";
    }



    public object updateWordList(dynamic parameters)
    {
      var userID = (Guid)Guid.Parse(parameters.userID.ToString());
      var listName = (string)parameters.name.ToString();
      var words = (List<Word>)JsonConvert.DeserializeObject<List<Word>>(parameters.words.ToString());
      var description = (string)parameters.description.ToString();
      var trainingListID = (Guid)Guid.Parse(parameters.SelectionCriteria.wordListID.ToString());

      using (var db = new DataConnection("Sql"))
      {
        var wordList = db.GetTable<TrainingList>()
                        .Where(c => c.ID == trainingListID)
                        .Set(c => c.Name, listName)
                        .Set(c => c.Description, description)
                        .Update();

        RemoveWordsFromList(words, trainingListID);
      }

      return trainingListID;
    }


    public void RemoveWordsFromList(List<Word> newWordList, Guid WordListID)
    {

      using(var db = new DataConnection("Sql"))
      {
        var oldWordList = db.GetTable<TrainingListWord>()
                            .LoadWith(c => c.Word)
                            .Where(c => c.TrainingListID == WordListID)
                            .ToList();

        foreach (var word in oldWordList)
        {
          if (!newWordList.Contains(word.Word))
          {
            var table = db.GetTable<TrainingListWord>().Where(c => c.WordID == word.WordID & c.TrainingListID == WordListID).Delete();

          }
        }

        var currentdWordList = db.GetTable<TrainingListWord>()
                                .LoadWith(c => c.Word)
                                .Where(c => c.TrainingListID == WordListID)
                                .ToList();

        AddWordsToList(currentdWordList, newWordList, WordListID);

      }
    }

    public void AddWordsToList(List<TrainingListWord> currentWordList, List<Word> newWordList, Guid WordListID)
    {

      using (var db = new DataConnection("Sql"))
      {

        var currentWordListIDs = currentWordList.Select(c => c.WordID);

        foreach (var newWord in newWordList)
        {
          if (!currentWordListIDs.Contains(newWord.ID))
          {
            var trainWord = new TrainingListWord()
            {
              ID = Guid.NewGuid(),
              TrainingListID = WordListID,
              WordID = newWord.ID
            };
            db.Insert(trainWord);
          }
        }
      }
    }


    public object deleteWordList(dynamic parameters)
    {
      var userID = (Guid)Guid.Parse(parameters.userID.ToString());
      var trainingListID = (Guid)Guid.Parse(parameters.SelectionCriteria.trainingListID.ToString());

      using (var db = new DataConnection("Sql"))
      {

        var traningListWord = db.GetTable<TrainingListWord>()
                              .Where(c => c.TrainingListID == trainingListID)
                              .Delete();

        var trainingList = db.GetTable<TrainingList>()
                            .Where(c => c.ID == trainingListID)
                            .Delete();

      }

      return true;
    }



    public object createWordList(dynamic parameters)
    {
      var userID = (Guid)Guid.Parse(parameters.userID.ToString());
      var listName = (string)parameters.name.ToString();
      var words = (List<Word>)JsonConvert.DeserializeObject<List<Word>>(parameters.words.ToString());
      var trainingListID = Guid.NewGuid();
      var description = (string)parameters.description.ToString();

      using (var db = new DataConnection("Sql"))
      {
        var trainingList = new TrainingList()
        {
          ID = trainingListID,
          Name = listName,
          DateTimeCreate = DateTimeOffset.Now,
          UserID = userID,
          Description = description
        };

        db.Insert(trainingList);

        //TODO: вынести привязку слов к списку в отдельный метод
        foreach (var word in words)
        {
          var wordID = db.GetTable<Word>().Where(w => w.En == word.En).Select(s => s.ID).FirstOrDefault();

          var trainingListWord = new TrainingListWord()
          {
            ID = Guid.NewGuid(),
            WordID = wordID,
            TrainingListID = trainingListID
          };

          db.Insert(trainingListWord);
        }
      }

      return true;
    }




    public object Get(dynamic parameters)
    {
      var result = new List<TrainingList>();
      var userID = (Guid)Guid.Parse(parameters.userID.ToString());



      using (var db = new DataConnection("Sql"))

      {

        var query = db.GetTable<TrainingList>()
                         .LoadWith(t => t.TrainingListWord[0].Word)
                         .AsQueryable();

        if (parameters.SelectionCriteria != null)
        {
          if (parameters.SelectionCriteria.wordListID != null)
          {
            var wordListID = (Guid)Guid.Parse(parameters.SelectionCriteria.wordListID.ToString());
            result = query.Where(c => c.ID == wordListID & c.UserID == userID).ToList();
          }
          else
          {
            result = query.Where(h => h.UserID == userID).ToList();
          }
        }
      }

      return result;
    }
  }
}
