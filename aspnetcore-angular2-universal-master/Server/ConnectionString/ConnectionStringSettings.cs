using LinqToDB.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Asp2017.Server.ConnectionString
{
  public class ConnectionStringSettings : IConnectionStringSettings
  {
    public string ConnectionString { get; set; }
    public string Name { get; set; }
    public string ProviderName { get; set; }
    public bool IsGlobal => false;
  }


  public class MySettings : ILinqToDBSettings
  {
    public IEnumerable<IDataProviderSettings> DataProviders
    {
      get { yield break; }
    }

    public string DefaultConfiguration => "SqlServer";
    public string DefaultDataProvider => "SqlServer";

    public IEnumerable<IConnectionStringSettings> ConnectionStrings
    {
      get
      {
        return (new List<ConnectionStringSettings>()
                    {

                        new ConnectionStringSettings
                        {
                            Name = "SqlServer",
                            ProviderName = "SqlServer",
                            ConnectionString = @"Server=localhost\SQLEXPRESS;Database=VocabularyBuilder;Trusted_Connection=True;"
                        },
                        new ConnectionStringSettings
                        {
                            Name = "Sql",
                            ProviderName = "SqlServer",
                            ConnectionString = @"Server=VADIMPC;Database=VocabularyBuilder;Trusted_Connection=True;"
                        }

            });
      }

    }
  }
}
