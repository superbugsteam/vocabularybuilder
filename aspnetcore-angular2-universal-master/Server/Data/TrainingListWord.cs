using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Asp2017.Server.Data
{
  public class TrainingListWord
  {
    [PrimaryKey, NotNull]
    public Guid ID { get; set; }

    [Column(Name = nameof(WordID)), NotNull]
    public Guid WordID { get; set; }

    [Column(Name = nameof(TrainingListID)), NotNull]
    public Guid TrainingListID { get; set; }

    [Association(ThisKey = nameof(WordID), OtherKey = nameof(Data.Word.ID))]
    public Word Word { get; set; }

  }
}
