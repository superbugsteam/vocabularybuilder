using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Asp2017.Server.Data
{
  public enum PartOfSpeechType
  {
    Существительное = 0,

    Прилагательное = 1,

    Глагол = 2,

    Наречие = 3,

    Числительное = 4,

    Местоимение = 5
  }
}
