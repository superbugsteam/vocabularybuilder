using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Asp2017.Server.Data
{
  
  public class Word
  {
    [PrimaryKey, NotNull]
    public Guid ID { get; set; }

    [Column(Name = nameof(Ru), Length = 100), NotNull]
    public string Ru { get; set; }

    [Column(Name = nameof(En), Length = 100), NotNull]
    public string En { get; set; }

    [Column(Name = nameof(PartOfSpeech), Length = 50), NotNull]
    public PartOfSpeechType PartOfSpeech { get; set; }

    [Column(Name = nameof(Anthonym), Length = 100), Nullable]
    public string Anthonym { get; set; }

    [Column(Name = nameof(Synonym), Length = 300), Nullable]
    public string Synonym { get; set; }

    [Column(Name = nameof(Plural), Length = 100), Nullable]
    public string Plural { get; set; }

    [Column(Name = nameof(Singular), Length = 100), Nullable]
    public string Singular { get; set; }

    [Column(Name = nameof(SecondForm), Length = 100), Nullable]
    public string SecondForm { get; set; }

    [Column(Name = nameof(ThirdForm), Length = 100), Nullable]
    public string ThirdForm { get; set; }

    [Column(Name = nameof(UsingExample), Length = 300), Nullable]
    public string UsingExample { get; set; }

    [Column(Name = nameof(EnDefinition), Length = 300), Nullable]
    public string EnDefinition { get; set; }

    [Column(Name = nameof(RuDefinition), Length = 300), Nullable]
    public string RuDefinition { get; set; }

    [Column(Name = nameof(DateTimeCreate)), NotNull]
    public DateTimeOffset? DateTimeCreate { get; set; }

    [Column(Name = nameof(UserID)), NotNull]
    public Guid UserID { get; set; }

  }
}
