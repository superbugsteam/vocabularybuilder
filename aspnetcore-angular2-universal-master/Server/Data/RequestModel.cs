using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Asp2017.Server.Data
{
  public class RequestModel
  {
    public string Method { get; set; }

    public dynamic Params { get; set; }
  }
}
