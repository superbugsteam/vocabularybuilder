using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Asp2017.Server.Data
{
  public class TrainingList
  {
    [PrimaryKey, NotNull]
    public Guid ID { get; set; }

    [Column(Name = nameof(Name), Length = 50), NotNull]
    public string Name { get; set; }

    [Column(Name = nameof(Capacity)), NotNull]
    public int Capacity { get; set; }

    [Column(Name = nameof(TrainingCount), Length = 100), Nullable]
    public int TrainingCount { get; set; }

    [Column(Name = nameof(Description), Length = 300), Nullable]
    public string Description { get; set; }

    [Column(Name = nameof(DateTimeCreate)), NotNull]
    public DateTimeOffset? DateTimeCreate { get; set; }

    [Column(Name = nameof(UserID)), NotNull]
    public Guid UserID { get; set; }

    [Association(ThisKey = nameof(ID), OtherKey = nameof(Data.TrainingListWord.TrainingListID))]
    public List<TrainingListWord> TrainingListWord { get; set; }
  }
}
