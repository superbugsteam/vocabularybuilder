import { Injectable, Inject, Injector} from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { ORIGIN_URL } from '@nguniversal/aspnetcore-engine/tokens';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class WordsService {

  private userID: string = "186a74ae-37fd-4a56-91ff-51d1f92d893c";
  private baseUrl: string;
  private _words: BehaviorSubject<Word[]>;
  private dataStore: {
    words: Word[]
  };

  constructor(private http: HttpClient, private injector: Injector) {
    this.baseUrl = this.injector.get(ORIGIN_URL);
    this.dataStore = { words: [] };
    this._words = <BehaviorSubject<Word[]>>new BehaviorSubject([]);
  }


  get words() {
    return this._words.asObservable();
  }

  wordAdd(original: string, translation: string, partOfSpeech: string, ndFormOfVerb: string, rdFormOfVerb: string, synonym: string, antonym: string, rusDescription: string, origDescription: string, examples: string) {
    let body = {
      method: "wordAdd",
      params: {
        userID: this.userID,
        original: original,
        translation: translation,
        partOfSpeech: partOfSpeech,
        ndFormOfVerb: ndFormOfVerb,
        rdFormOfVerb: ndFormOfVerb,
        synonym: ndFormOfVerb,
        antonym: ndFormOfVerb,
        rusDescription: ndFormOfVerb,
        origDescription: ndFormOfVerb,
        examples: ndFormOfVerb
      }
    };
    let str = JSON.stringify(body);

    return this.http.post(`${this.baseUrl}/api/Words`, str, httpOptions);
  }

  getWords(filter = "", sortOrder = 'asc'): Observable<Word[]> {
    let body = {
      method: "get",
      params: {
        SelectionCriteria: {
          filter: filter
        },
        sortOrder: sortOrder,
        userID: this.userID,
      }
    };

    let str = JSON.stringify(body);
    return <Observable<Word[]>>this.http.post(`${this.baseUrl}/api/Words`, str, httpOptions);
    
    //subscribe(data => {
    //  this.dataStore.words = <Word[]>data;
    //  this._words.next(Object.assign({}, this.dataStore).words);
    //}, error => console.log('Could not load words.'));
  }


  getWords2(filter = "", sortOrder = 'asc'){
    let body = {
      method: "get",
      params: {
        SelectionCriteria: {
          filter: filter
        },
        sortOrder: sortOrder,
        userID: this.userID,
      }
    };

    let str = JSON.stringify(body);
    this.http.post(`${this.baseUrl}/api/Words`, str, httpOptions).subscribe(data => {
      this.dataStore.words = <Word[]>data;
      this._words.next(Object.assign({}, this.dataStore).words);
    }, error => console.log('Could not load words.'));
  }


  //search(word: string) {
  //  console.log(word)
  //  let body = {
  //    method: "search",
  //    params: {
  //      SelectionCriteria: {
  //        word: word
  //      },
  //      userID: this.userID,
  //    }
  //  };
  //  let str = JSON.stringify(body);
  //  return this.http.post(`${this.baseUrl}/api/Words`, str, httpOptions);
  //}
}

 
export class Word {
  id: string;
  ru: string;
  en: string;
  anthonym: string;
  synonym: string;
  partOfSpeech: string;
  plural: string;
  singular: string;
  secondForm: string;
  thirdForm: string;
  usingExample: string;
  enDefinition: string;
  ruDefinition: string;
  dateTimeCreate: Date;
  userID: string;
}
