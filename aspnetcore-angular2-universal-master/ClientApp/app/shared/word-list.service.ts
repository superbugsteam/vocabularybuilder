import { Injectable, Inject, Injector } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { ORIGIN_URL } from '@nguniversal/aspnetcore-engine/tokens';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class WordListService {
  private userID: string = "186a74ae-37fd-4a56-91ff-51d1f92d893c";
  private baseUrl: string;
  private _wordLists: BehaviorSubject<TrainingList[]>;
  private dataStore: {
    wordLists: TrainingList[]
  };

  constructor(private http: HttpClient, private injector: Injector) {
    this.baseUrl = this.injector.get(ORIGIN_URL);
    this.dataStore = { wordLists: [] };
    this._wordLists = <BehaviorSubject<TrainingList[]>>new BehaviorSubject([]);
  }

  get wordLists() {
    return this._wordLists.asObservable();
  }


  getWordLists() {
    let body = {
      method: "get",
      params: {
        SelectionCriteria: {
          
        },
        userID: this.userID,
      }
    };

    let str = JSON.stringify(body);
    this.http.post(`${this.baseUrl}/api/WordList`, str, httpOptions).subscribe(data => {
      this.dataStore.wordLists = <TrainingList[]>data;
      this._wordLists.next(Object.assign({}, this.dataStore).wordLists);
    }, error => console.log('Could not load wordLists.'));
  }



  getOneWordList(wordListID: string) {
    let body = {
      method: "get",
      params: {
        SelectionCriteria: {
          wordListID: wordListID
        },
        userID: this.userID,
      }
    };
    let str = JSON.stringify(body);
    return this.http.post(`${this.baseUrl}/api/WordList`, str, httpOptions);
  }

  createWordList(name: string, words: Word[], description: string) {
    let body = {
      method: "createWordList",
      params: {
        userID: this.userID,
        name: name,
        words: words,
        description: description
      }
    };
    let str = JSON.stringify(body);

    return this.http.post(`${this.baseUrl}/api/WordList`, str, httpOptions);

  }

  updateWordList(name: string, words: Word[], description: string,  wordListID: string) {
    let body = {
      method: "updateWordList",
      params: {
        SelectionCriteria: {
          wordListID: wordListID
        },
        userID: this.userID,
        name: name,
        words: words,
        description: description
      }
    };
    let str = JSON.stringify(body);

    return this.http.post(`${this.baseUrl}/api/WordList`, str, httpOptions);

  }



  deleteWordList(trainingListID: string) {
    let body = {
      method: "deleteWordList",
      params: {
        SelectionCriteria: {
          trainingListID: trainingListID
        },
        userID: this.userID
      }
    };
    let str = JSON.stringify(body);

    return this.http.post(`${this.baseUrl}/api/WordList`, str, httpOptions);
  }

}


export interface TrainingList {
  id: string;
  name: string;
  capacity: number;
  dateTimeCreate: Date;
  trainingCount: number;
  userID: string;
  description: string;
}


export interface Word {
  id: string;
  ru: string;
  en: string;
  anthonym: string;
  synonym: string;
  partOfSpeech: string;
  plural: string;
  singular: string;
  secondForm: string;
  thirdForm: string;
  usingExample: string;
  enDefinition: string;
  ruDefinition: string;
  dateTimeCreate: Date;
  userID: string;
}
