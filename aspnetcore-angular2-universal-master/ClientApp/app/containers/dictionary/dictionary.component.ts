import { Component, OnInit, ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import { WordsService } from '../../shared/words.service';
import { DataSource, CollectionViewer } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { debounceTime, distinctUntilChanged, tap, finalize, catchError } from 'rxjs/operators';
import { BehaviorSubject, of, merge } from 'rxjs';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { MatSort } from '@angular/material';



@Component({
  selector: 'dictionary',
  templateUrl: './dictionary.component.html',
  providers: [WordsService],
  styleUrls: ['./table-sorting-example.css']
})
export class DictionaryComponent implements OnInit, AfterViewInit  {

  dataSource: WordsDataSource;
  displayedColumns = ['en', 'ru', 'partOfSpeech'];

  @ViewChild('inputWord') inputWord: ElementRef;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private wordsService: WordsService) {
  }


  ngOnInit() {
    this.dataSource = new WordsDataSource(this.wordsService);
    this.dataSource.getWordsFromService();
  }

  ngAfterViewInit() {

    this.sort.sortChange
      .pipe(
         tap(() => this.loadWords())
      )
      .subscribe();



    fromEvent(this.inputWord.nativeElement, 'keyup')
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
      tap(() => {
        this.loadWords();
        })
      )
      .subscribe();
  }


  loadWords() {
    this.dataSource.getWordsFromService(this.inputWord.nativeElement.value, this.sort.direction)
  }
}


export class WordsDataSource implements DataSource<any> {

  constructor(private wordsService: WordsService) { }

  private wordsSubject = new BehaviorSubject<Word[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();


  connect(collectionViewer: CollectionViewer): Observable<Word[]> {
    return this.wordsSubject.asObservable();       
  }


  disconnect(collectionViewer: CollectionViewer): void {
    this.wordsSubject.complete();
    this.loadingSubject.complete();
  }


  getWordsFromService(filter = "", sortOrder = 'asc')
  {
    this.loadingSubject.next(true);
    this.wordsService.getWords(filter, sortOrder)
      .pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false)))
            .subscribe(words => this.wordsSubject.next(words));
  }
}



export interface Word {
  id: string;
  ru: string;
  en: string;
  anthonym: string;
  synonym: string;
  partOfSpeech: string;
  plural: string;
  singular: string;
  secondForm: string;
  thirdForm: string;
  usingExample: string;
  enDefinition: string;
  ruDefinition: string;
  dateTimeCreate: Date;
  userID: string;
}

