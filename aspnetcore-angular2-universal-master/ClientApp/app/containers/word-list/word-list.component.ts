import { Component, OnInit } from '@angular/core';
import { WordsService } from '../../shared/words.service';
import { WordListService } from '../../shared/word-list.service';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'word-list',
  templateUrl: './word-list.component.html',
  providers: [WordsService, WordListService],
})
export class WordListComponent implements OnInit {

  wordLists: TrainingList[];

  constructor(private wordListService: WordListService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.wordListService.getWordLists();
    this.wordListService.wordLists.subscribe(updatedNotifications => {
      this.wordLists = updatedNotifications;
    });
  }


  navigateAdd() {
    this.router.navigate(["/word-list-action"], {
      queryParams: {
        mode: 'add'
      }, relativeTo: this.route
    });
  }

  navigateUpdate(id: string) {
    this.router.navigate(["/word-list-action"], {
      queryParams: {
        mode: 'update',
        id: id
      }, relativeTo: this.route
    });
  }


  deleteTrainingList(trainingListID: string) {
    this.wordListService.deleteWordList(trainingListID).subscribe(data => {
      this.wordListService.getWordLists();
    }, error => console.log('Could not delete trainingList.'));
  }


}


export interface TrainingList {
  id: string;
  name: string;
  capacity: number;
  dateTimeCreate: Date;
  trainingCount: number;
  userID: string;
  description: string;
}
