import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { WordsService, Word } from '../../shared/words.service';
import { WordListService } from '../../shared/word-list.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from "@angular/router";
import { SelectionModel } from '@angular/cdk/collections';
import { MatListOption } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';


@Component({
  selector: 'word-list-action',
  templateUrl: './word-list-action.component.html',
  providers: [WordsService, WordListService],
})
export class WordListActionComponent implements OnInit {

  @ViewChild('selectedWords') selectedWords: ElementRef;


  private querySubscr: Subscription;
  words: Word[];
  public mode: string;
  trainingListWord: TrainingList[];
  wL: TrainingListWord[];
  wordListID: string;
  wordListForm: FormGroup;
  public selectedOptions: Array<string>;
  tempraryWord: Word;

  constructor(private fb: FormBuilder, private wordService: WordsService, private router: Router, private wordListService: WordListService, private route: ActivatedRoute) {
 
  
  }


  ngOnInit() {
    
    this.initForm();

    this.wordService.getWords2();
    this.wordService.words.subscribe(serviceWords => {
      this.words = serviceWords;
      console.log('Все слова ' + this.words);

      this.querySubscr = this.route.queryParams.subscribe(queryParam => {
        this.mode = queryParam['mode'];
        if (this.mode == 'update') {
          this.wordListID = queryParam['id'];
          this.initialyzeWordList(this.wordListID);
        }
      });
    });
  }


  public initForm() {
    
   

    this.wordListForm = this.fb.group({
      wordListName: [''],
      wordListDescription: [''],
    })
  }



  initialyzeWordList(id: string) {
    this.wordListService.getOneWordList(id).subscribe(wordList => {
      this.trainingListWord = <TrainingList[]>wordList;
      this.wordListForm.controls['wordListName'].setValue(this.trainingListWord[0].name);
      this.wordListForm.controls['wordListDescription'].setValue(this.trainingListWord[0].description);
      this.selectedOptions = this.getSelectedObjects();
      // this.wordListForm.controls['wordsSelect'].setValue(this.getSelectedObjects());
    });
  }


  public getSelectedObjects(): Array<string> {
    let arrayOfobjects = new Array()
    for (var i = 0; i < this.trainingListWord.length; i++) {
      this.wL = this.trainingListWord[i].trainingListWord;
      for (var j = 0; j < this.wL.length; j++) {
        arrayOfobjects.push(this.wL[j].word.en)
      }
    }
    console.log('Массив слов тренировочного листа: ' + arrayOfobjects)
    return arrayOfobjects;
  }



  //метод  сравнивает word.en со списком всех слов и если совпадает, то записывает совпадающий объект в массив arrayOfWordToUpload
  UploadSelectedObjects(): Array<Word> {
    console.log('Слова для сохранения ' + this.selectedOptions)
    let AllWords = this.words;
    let arrayOfWordToUpload = Array<Word>();
    for (let i = 0; i < AllWords.length; i++) {
      let tempWord = AllWords[i];
      console.log(tempWord)
      for (let j = 0; j < this.selectedOptions.length; j++) {
        if (this.selectedOptions[j] == tempWord.en) {
          arrayOfWordToUpload.push(tempWord);
        }
      }
    }
    console.log("Слова для сохранения: " + arrayOfWordToUpload);
    return arrayOfWordToUpload;
  }


  createWordList() {
    let name = this.wordListForm.controls['wordListName'].value;
    let description = this.wordListForm.controls['wordListDescription'].value;
    let words = this.UploadSelectedObjects();
    this.wordListService.createWordList(name, words, description).subscribe(data => {
      this.wordListService.getWordLists();
      this.navigateBack();
    }, error => console.log('Could not create wordLists.'));
  }


  updateWordList() {
    let name = this.wordListForm.controls['wordListName'].value;
    let description = this.wordListForm.controls['wordListDescription'].value;
    console.log("Вот слова " + this.words)
    // let words = this.wordListForm.controls['wordsSelect'].value;
    let words = this.UploadSelectedObjects();
    console.log("Вы выбрали: " + words);
    this.wordListService.updateWordList(name, words, description, this.wordListID).subscribe(data => {
      this.navigateBack();
    }, error => console.log('Could not update wordLists.'));
  }


  public navigateBack() {
    this.router.navigate(["/word-list"]);
  }


}



export interface Word {
  id: string;
  ru: string;
  en: string;
  anthonym: string;
  synonym: string;
  partOfSpeech: number;
  plural: string;
  singular: string;
  secondForm: string;
  thirdForm: string;
  usingExample: string;
  enDefinition: string;
  ruDefinition: string;
  dateTimeCreate: Date;
  userID: string;
}

export interface TrainingList {
  id: string;
  name: string;
  capacity: number;
  dateTimeCreate: Date;
  trainingCount: number;
  userID: string;
  description: string;
  trainingListWord: TrainingListWord[]
}


export interface TrainingListWord {
  id: string;
  wordID: string;
  trainingListID: string;
  word: Word;
}
