import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { WordsService } from '../../shared/words.service';
import { Router } from '@angular/router';


@Component({
  selector: 'word-add',
  templateUrl: './word-add.component.html',
  providers: [WordsService],
})

export class WordAddComponent implements OnInit {
  partsOfSpeech: Array<string> = ['глагол', 'существительное'];
  wordAddListForm: FormGroup;
  selectedPartOfSpeech: string;
  constructor(private fb: FormBuilder, private wordService: WordsService, private router: Router) { }

  ngOnInit() {
    this.initForm();
  }
  ngAfterViewInit() {
    this.selectedPartOfSpeech = this.wordAddListForm.controls['selectedPartOfSpeech'].value;
  }
  public initForm() {
    this.wordAddListForm = this.fb.group({
      original: [''],
      translation: [''],
      selectedPartOfSpeech: [''],
      ndFormOfVerb: [''],
      rdFormOfVerb: [''],
      synonym: [''],
      antonym: [''],
      rusDescription: [''],
      origDescription: [''],
      examples: [''],
    })
  } 

  wordAdd() {
    let original = this.wordAddListForm.controls['original'].value;
    let translation = this.wordAddListForm.controls['translation'].value;
    let partOfSpeech = this.wordAddListForm.controls['selectedPartOfSpeech'].value;
    let ndFormOfVerb = this.wordAddListForm.controls['ndFormOfVerb'].value;
    let rdFormOfVerb = this.wordAddListForm.controls['rdFormOfVerb'].value;
    let synonym = this.wordAddListForm.controls['synonym'].value;
    let antonym = this.wordAddListForm.controls['antonym'].value;
    let rusDescription = this.wordAddListForm.controls['rusDescription'].value;
    let origDescription = this.wordAddListForm.controls['origDescription'].value;
    let examples = this.wordAddListForm.controls['examples'].value;
    this.wordService.wordAdd(original, translation, partOfSpeech, ndFormOfVerb, rdFormOfVerb, synonym, antonym, rusDescription, origDescription, examples).subscribe(data => {
      this.navigateBack();
    }, error => console.log('Could not create wordLists.'));
    console.log(partOfSpeech);
  }

  public navigateBack() {
    this.router.navigate(["/dictionary"]);
  }
}



